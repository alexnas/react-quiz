This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# React Quiz

This React Redux application is designed to create simple quizes. Consumes data from the Firebase API. Registered admin can create a quiz. Unregistered users can choose a quiz from a list and pasar it.

## Uses:

- React
- Redux
- React Router
- Redux Thunk middleware
- Firebase API on the backend
- Login/password authentication through Firebase API
- Firebase data storage

## Installation and usage

- Be sure that Git and Node.js are installed globally.
- Clone this repo.
- Run `npm install`, all required components will be installed automatically.
- Run `npm start` to start the project.
- Run `npm build` to create a build directory with a production build of the app.
- Run `npm test` to test the app.

## License

This project is licensed under the terms of the MIT license.
