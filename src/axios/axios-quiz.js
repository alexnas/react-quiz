import axios from "axios";

export default axios.create({
  baseURL: "https://react-quiz-6d298.firebaseio.com"
});
